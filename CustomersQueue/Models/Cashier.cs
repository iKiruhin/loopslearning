﻿using System;
using System.Threading;

namespace CustomersQueue.Models
{
    public class Cashier
    {
        string Label { get; }

        public Cashier(string label)
        {
            Label = label;
        }

        private void HandleCustomerGoods(Customer customer)
        {
            Console.WriteLine($"добавляем товары в чек для {customer.Label}");
        }

        private void AskAboutPackage(Customer customer)
        {
            Console.WriteLine($"спрашиваем: нужен ли пакет для {customer.Label}");
        }

        private void ReceivePayment(Customer customer)
        {
            Console.WriteLine($"принимаем оплату от {customer.Label}");
        }

        private void ReturnInvoice(Customer customer)
        {
            Console.WriteLine($"выдаем чек для {customer.Label}");
        }


        public void DoWork()
        {
            Console.WriteLine($"    Оператор {Label} занят, оставайтесь на линии...");
            Thread.Sleep(1000);
        }

        public void GoTakeABreak()
        {
            Console.WriteLine($"    Оператор {Label} ушел на обед. Касса временно не работает.");
        }

        public void GoHomeByEndOfJob()
        {
            Console.WriteLine(
                $"    Оператор {Label} говорит: 'Ура, очередь закончилась! Пойду домой.'");
        }

        public void GoHomeByEndOfTime()
        {
            Console.WriteLine(
                $"    Оператор {Label} говорит: 'Ура, рабочий день закончился! Всем спасибо, все свободны!'");
        }

        public void ServeCustomer(Customer customer)
        {
            Console.WriteLine($"    Оператор {Label} обслуживает нового покупателя.");
            HandleCustomerGoods(customer);
            AskAboutPackage(customer);
            ReceivePayment(customer);
            ReturnInvoice(customer);
        }

        public bool IsEndOfDay(DateTime currentDateTime)
        {
            Console.Write(currentDateTime.ToShortTimeString() + " ");

            DateTime endOfWorkDay = DateTime.Today;
            endOfWorkDay = endOfWorkDay.AddHours(21); // set time to 21:00
            endOfWorkDay = endOfWorkDay.AddMinutes(00); // set time to 21:00

            return DateTime.Compare(currentDateTime, endOfWorkDay) <= 0;
        }

        public DateTime ServeCustomerSlowly(Customer customer, DateTime currentDateTime)
        {
            ServeCustomer(customer);
            double serveTime = 5;
            return currentDateTime.AddMinutes(serveTime);
        }

        public DateTime WaitAwhile(DateTime currentDateTime)
        {
            Console.WriteLine($"    Оператор {Label} ждет следующего клиента");
            return currentDateTime.AddMinutes(1);
        }
    }
}