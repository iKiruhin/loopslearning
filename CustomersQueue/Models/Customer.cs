﻿namespace CustomersQueue.Models
{
    public class Customer
    {
        public Customer()
        {
        }

        public Customer(string label)
        {
            Label = label;
        }

        public string Label { get; set; }
    }
}
