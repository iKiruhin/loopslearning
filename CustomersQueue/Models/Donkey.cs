﻿using System;

namespace CustomersQueue.Models
{
    public class Donkey
    {
        int milesPassed = 0;
        string name { get; }

        public Donkey(string name)
        {
            this.name = name;
        }

        public void MoveOneMileFurther()
        {
            milesPassed = milesPassed + 1;
  
            Console.WriteLine($"{name} проехал еще милю ({milesPassed}-ю)...");
        }

        public void EventuallyWeHaveCome()
        {
            Console.WriteLine($"{name}: 'Ну наконец-то приехали!'");
        }

        public bool AskAreWeThereYet()
        {
            bool areWeThereYet = milesPassed < 5;
            string ridersAnswer = !areWeThereYet ? "Да" : "Нет";
         
            Console.WriteLine($"{name} спрашивает: 'Мы уже приехали?'");
            Console.WriteLine($"Кучер отвечает: '{ridersAnswer}'");

            return areWeThereYet;
        }
    }
}