﻿using System;
using System.Collections.Generic;
using CustomersQueue.Models;

namespace CustomersQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Customer> customersQueue =
                new List<Customer>
                {
                    new Customer("Rich Man"),
                    new Customer("Poor Student"),
                    new Customer("A Housewife"),
                    new Customer("A Kid"),
                };

            Cashier rapidCashier = new Cashier("Шустрый кассир");

//            RunForeachLoop(customersQueue, rapidCashier);
//            RunPreconditionalLoop(customersQueue, rapidCashier);
            RunPreconditionalLoop2(customersQueue, rapidCashier);
//            RunPostconditionalLoop(customersQueue, rapidCashier);
//            RunInfiniteLoop(rapidCashier);
        }

        private static void RunForeachLoop(List<Customer> customersQueue, Cashier rapidCashier)
        {
            foreach (Customer customer in customersQueue)
            {
                rapidCashier.ServeCustomer(customer);
            }

            rapidCashier.GoTakeABreak();
        }

        private static void RunPreconditionalLoop(List<Customer> customersQueue, Cashier rapidCashier)
        {
            IEnumerator<Customer> cusomersEnumerator = customersQueue.GetEnumerator();

            while (cusomersEnumerator.MoveNext())
            {
                Customer customer = cusomersEnumerator.Current;
                rapidCashier.ServeCustomer(customer);
            }

            rapidCashier.GoHomeByEndOfJob();
        }

        private static void RunPreconditionalLoop2(List<Customer> customersQueue, Cashier rapidCashier)
        {
            IEnumerator<Customer> cusomersEnumerator = customersQueue.GetEnumerator();
            DateTime currentDateTime = DateTime.Today;
            currentDateTime = currentDateTime.AddHours(20); // set time to 20:50
            currentDateTime = currentDateTime.AddMinutes(50); // set time to 20:50

            while (rapidCashier.IsEndOfDay(currentDateTime))
            {
                if (!cusomersEnumerator.MoveNext())
                {
                    currentDateTime = rapidCashier.WaitAwhile(currentDateTime);
                    continue;
                }

                Customer customer = cusomersEnumerator.Current;
                currentDateTime = rapidCashier.ServeCustomerSlowly(customer, currentDateTime);
            }

            rapidCashier.GoHomeByEndOfTime();
        }

        private static void RunPostconditionalLoop(List<Customer> customersQueue, Cashier rapidCashier)
        {
            Donkey impatientDonkey = new Donkey("Нетерпеливый ослик");

            do
            {
                impatientDonkey.MoveOneMileFurther();
            } while (impatientDonkey.AskAreWeThereYet());

            impatientDonkey.EventuallyWeHaveCome();
        }

        private static void RunInfiniteLoop(Cashier cashier)
        {
            while (true)
            {
                cashier.DoWork();
            }
        }
    }
}